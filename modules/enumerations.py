from enum import Enum, unique


@unique
class SensorStatus(Enum):
    NORMAL = 0
    LOW = 1
    HIGH = 2
    CRITICAL_LOW = 3
    CRITICAL_HIGH = 4


@unique
class PercomTypeEnum(Enum):
    SENSOR = 0
    RELAY = 1


@unique
class RelayChannelTypeEnum(Enum):
    FAN = 0
    LIGHT = 1
    PUMP = 2


@unique
class RelayModuleStatusEnum(Enum):
    OPEN = 0
    CLOSED = 1


@unique
class SensorTypeEnum(Enum):
    AQUA = 0
    CARBONE_DIOXIDE = 1
    HUMIDITY = 2
    LIGHTING = 3
    TEMPERATURE = 4


@unique
class TimeOfADay(Enum):
    F24H = 0
    DAY = 1
    NIGHT = 2
