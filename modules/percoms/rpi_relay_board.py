"""Модуль подключения блоков реле WaveShare:
https://www.waveshare.com/rpi-relay-board.htm
https://www.waveshare.com/rpi-relay-board-b.htm

Каналы перечислены по порядку: CH1, CH2, CH3, ... ,CHn.

AB - power on state (1 - on).
BC - default state (0 - off).
"""

import RPi.GPIO as GPIO

from modules.enumerations import RelayChannelTypeEnum, RelayModuleStatusEnum, PercomTypeEnum
from modules.interfaces import IDriver, IRelayBoard
from typing import Dict, List


class Driver(IDriver, IRelayBoard):
    """ Класс Класс управления модулем реле.
    Конструктор принимает тип реле.
    Поддерживаемые типы релейных модулей: 'rpi', 'rpi-b'.
    """

    _supported_devices = ('rpi', 'rpi-b')

    # Модели модулей с пинами установленных реле.
    _relay_board_pins = {
        'rpi': (26, 20, 21),
        'rpi-b': (5, 6, 13, 16, 19, 20, 21, 26)
    }

    # Словарь соответствия периферийных устройств индексам пинов.
    _relay_channel_type_to_pin_index_map: Dict[int, RelayChannelTypeEnum] = {}

    def __init__(self, board: str, channel_type: Dict[str, str]) -> None:
        if GPIO.getmode() != GPIO.BCM:
        GPIO.setmode(GPIO.BCM)  # GPIO init.

        try:
            self._pins = self._relay_board_pins[board]
        except KeyError as e:
            print(f"Invalid board type {board}:", e)
            exit(1)

        self._pin_quantity = len(self._pins)

        GPIO.setup(self._pins, GPIO.OUT)
        self._map_devices_to_pin_indexes(channel_type)

    def _get_pin_index_from_type(self, channel_type: RelayChannelTypeEnum) -> List:
        """Метод возвращает список индексов пинов, к которым подключено
         устройство указанного типа.
         """
        pin_indexes = []

        for p, t in self._relay_channel_type_to_pin_index_map.items():
            if t == channel_type:
                pin_indexes.append(p)

        return pin_indexes

    def _map_devices_to_pin_indexes(self, device_channels: Dict[str, str]) -> None:
        """Метод сопоставляет подключенные устройства с индексами
        пинов.
        """

        # Соответствие типов подключенных устройств.
        channel_type_list = {
            'fan': RelayChannelTypeEnum.FAN,
            'light': RelayChannelTypeEnum.LIGHT,
            'pump': RelayChannelTypeEnum.PUMP
        }

        for ch, t in device_channels.items():
            pin = 0
            for c in ch:
                if c.isdigit():
                    pin += int(c)

            self._relay_channel_type_to_pin_index_map[pin - 1] = channel_type_list[t]

    def get_relay_channels_quantity(self) -> int:
        return self._pin_quantity

    def get_relay_channel_status(self, channel: int) -> RelayModuleStatusEnum:
        """Метод возвращает состояние канала реле:
        CLOSED (1 (True): GPIO.HIGH);
        OPEN (0 (False): GPIO.LOW).
        """
        if not channel > self._pin_quantity and channel > 0:
            return RelayModuleStatusEnum(GPIO.input(channel - 1))
        else:
            print(f"Такого канала нет {channel}")
            exit(1)

    def get_relay_channel_type(self, channel: int) -> RelayChannelTypeEnum:
        """Метод возвращает тип подключённого к каналу устройства."""

        if channel < 1 or channel > len(self._relay_channel_type_to_pin_index_map):
            print(f"Такого канала не существует: {channel}")
            exit(1)

        return self._relay_channel_type_to_pin_index_map[channel - 1]

    def get_channel_type_list(self, channel_type: RelayChannelTypeEnum) -> List[int]:
        return list(map(lambda x: x + 1, self._get_pin_index_from_type(channel_type)))

    def get_type(self) -> PercomTypeEnum:
        return PercomTypeEnum.RELAY

    def set_relay_channel_status(self, channel: int, status: RelayModuleStatusEnum) -> None:
        if not channel > self._pin_quantity and channel > 0:
            GPIO.output(self._pins[channel - 1], status.value)  # (Переключение реле)
        else:
            print(f"Такого канала нет {channel}")
            exit(1)

    def toggle_relay_channel(self, channel: int) -> None:
        """Метод переключает релейный канал."""
        if not channel > self._pin_quantity and channel > 0:
            status = not self.get_relay_channel_status(channel)
            GPIO.output(self._pins[channel - 1], status)  # (Переключение реле)
        else:
            print(f"Такого канала нет {channel}")
            exit(1)

    def __del__(self):
        GPIO.cleanup()
