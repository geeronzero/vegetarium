"""Модуль подключения датчика температуры и влажности DHT11,
DHT22 (AM2302).

В случае, если появляется ошибка:
 File "/usr/local/lib/python3.7/dist-packages/Adafruit_DHT/Beaglebone_
 Black.py", line 24, in <module>
 from . import Beaglebone_Black_Driver as driver
 ImportError: cannot import name 'Beaglebone_Black_Driver' from
'Adafruit_DHT' (/usr/local/lib/python3.7/dist-packages/Adafruit_DHT/
__init__.py)

необходимо в файл:
 /usr/local/lib/python3.7/dist-packages/Adafruit_DHT/platform_detect.py

после строки #112 добавить следующий блок elif:

elif match.group(1) == 'BCM2711':
    return 3

"""
from typing import Union

import Adafruit_DHT

from modules.enumerations import SensorTypeEnum, PercomTypeEnum
from modules.interfaces import IDriver, ISensors


class Driver(IDriver, ISensors):
    """ Класс обрабатывает информацию из сенсоров DHT.
    Конструктор принимает тип сенсора и номер контакта, к которому
    подключен датчик. (см. команду pinout). Поддерживаемые типы
    сенсоров: 'DHT11', 'DHT22'.
    """

    _supported_devices = ('DHT11', 'DHT22')

    def __init__(self, sensor: str, pin: int):
        if sensor.upper() in self._supported_devices:
            f = getattr(Adafruit_DHT, sensor)
            self._dht_device = f()
        else:
            print("Некорректный тип датчика: ", sensor)
            exit(1)

        self._pin = pin

    def __str__(self):
        info = "Устройство: {0};\nПин подключения: {1};\nТемпература: {2};\n" \
               "Влажность: {3}"\
            .format(self._dht_device, self._pin, self.get_temperature(),
                    self.get_humidity())
        return info

    def get_humidity(self):
        return self.get_data()[0]

    def get_temperature(self):
        return self.get_data()[1]

    def get_data(self):
        h, t = Adafruit_DHT.read_retry(self._dht_device, self._pin)
        return h, t

    def get_type(self) -> PercomTypeEnum:
        return PercomTypeEnum.SENSOR

    def get_sensor_type(self) -> Union[SensorTypeEnum, list[SensorTypeEnum]]:
        return [SensorTypeEnum.HUMIDITY, SensorTypeEnum.TEMPERATURE]
