from abc import ABC, abstractmethod
from modules.enumerations import PercomTypeEnum, SensorTypeEnum,\
    RelayModuleStatusEnum, RelayChannelTypeEnum, SensorStatus
from typing import Dict, List, Tuple, Union


class IDriver(ABC):
    @abstractmethod
    def __init__(self, model: str, pin: Union[int, Tuple[Union[int]], None]) -> None:
        pass

    @abstractmethod
    def get_type(self) -> PercomTypeEnum:
        pass


class ISensors(ABC):
    @abstractmethod
    def get_sensor_type(self) -> Union[SensorTypeEnum, list[SensorTypeEnum]]:
        pass

    @abstractmethod
    def get_data(self):
        pass


class IRelayBoard(ABC):
    @abstractmethod
    def __init__(self, channel_type: Dict[str, str]):
        pass

    @abstractmethod
    def get_relay_channels_quantity(self) -> int:
        pass

    @abstractmethod
    def get_relay_channel_type(self, channel: int) -> RelayChannelTypeEnum:
        pass

    @abstractmethod
    def get_channel_type_list(self, channel_type: RelayChannelTypeEnum) -> List[int]:
        pass

    @abstractmethod
    def get_relay_channel_status(self, channel: Union[RelayChannelTypeEnum, int]) -> RelayModuleStatusEnum:
        pass

    @abstractmethod
    def set_relay_channel_status(self, channel: Union[RelayChannelTypeEnum, int],
                                 status: RelayModuleStatusEnum) -> None:
        pass

    @abstractmethod
    def toggle_relay_channel(self, channel: Union[RelayChannelTypeEnum, int]) -> None:
        pass


class ISensorHandler(ABC):
    @abstractmethod
    def __init__(self, sensor_object: ISensors) -> None:
        pass

    @abstractmethod
    def check_status(self) -> SensorStatus:
        pass


class SingletonBaseClass(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(SingletonBaseClass, cls).\
                __call__(*args, **kwargs)
        return cls._instances[cls]
