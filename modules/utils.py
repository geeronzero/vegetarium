import csv
import importlib
import sys
from time import localtime, mktime, sleep, time
from typing import Dict, List, Tuple

import config
from modules.interfaces import IDriver, SingletonBaseClass
from modules.enumerations import PercomTypeEnum, SensorTypeEnum, \
    RelayChannelTypeEnum, TimeOfADay


class IOUtils(metaclass=SingletonBaseClass):
    """Класс используется для чтения и записи данных."""
    _globals_var: dict

    def __init__(self, globals_var: dict):
        self._globals_var = globals_var

    @staticmethod
    def csv_reader(file_name: str, delimiter: str = ','):
        """ Функция, читающая csv-файлы. В качестве аргументов принимает
        имя файла и разделитель."""
        final_dict = dict()

        with open(file_name) as file_object:
            reader = csv.DictReader(file_object, delimiter=delimiter)

            # Преобразование в словарь списков.
            for line in reader:
                for k, v in line.items():
                    if k not in final_dict:
                        final_dict[k] = []
                    final_dict[k].append(v)

        return final_dict

    def import_modules(self, modules_set: set, parent_path: str) -> None:
        """Функция импорта модулей. Принимает список импортируемых модулей,
        каталог, содержащий модули, а так же глобальную переменную globals.
        """
        for module in modules_set:
            try:
                self._globals_var[module] = importlib.import_module("." + module, parent_path)
            except ImportError as e:
                print(f"Failed to import module {module}:", e)
                sys.exit(1)


class PercomsHandler:
    """ Класс управления периферийными устройствами."""
    _num_of_tiers: int

    # Соответствие устройств модулям.
    _module_match_list = {
        'dht11': 'dht',
        'dht22': 'dht',
        'mh_z19': 'mh_z19',
        'rpi': 'rpi_relay_board',
        'rpi-b': 'rpi_relay_board',
    }

    # Списки периферийных устройств.
    _sensors_list = {
        SensorTypeEnum.AQUA: [],
        SensorTypeEnum.CARBONE_DIOXIDE: [],
        SensorTypeEnum.HUMIDITY: [],
        SensorTypeEnum.LIGHTING: [],
        SensorTypeEnum.TEMPERATURE: []
    }

    _relays_list = {
        RelayChannelTypeEnum.FAN: [],
        RelayChannelTypeEnum.LIGHT: [],
        RelayChannelTypeEnum.PUMP: []
    }

    _tier_objects_list = {}

    _globals_var: dict
    _io_utils: IOUtils

    def __init__(self, globals_var: dict, io_utils: IOUtils):
        self._globals_var = globals_var
        self._io_utils = io_utils

        self._percoms_directory = config.percoms_directory
        self._percoms_installed = config.percoms_installed
        self._num_of_tiers = config.num_of_tiers

        self.load_percoms()

        # Распределение объектов периферийных устройст по ярусам.
        for e in SensorTypeEnum:
            if self._sensors_list[e]:
                self._tier_objects_list[e] = self._distribute_percoms(self._sensors_list[e])

        for e in self._relays_list:
            if self._relays_list[e]:
                self._tier_objects_list[e] = self._distribute_percoms(self._relays_list[e])

    def _get_percom_list(self):  # is_in_set
        """Функция, возвращающая список модулей к загрузке."""
        load_module_set = set()

        for percom in self._percoms_installed:
            load_module_set.add(self._module_match_list.get(percom[0]))

        return load_module_set

    def _calculate_percom_portions(self, percoms_amount: int) -> List[int]:
        """Метод вычисляет число ярусов на модуль, либо число модулей на ярус."""
        if not percoms_amount > 0:
            return [0]

        if percoms_amount > self._num_of_tiers:
            num_of_tiers = percoms_amount
            percoms_amount = self._num_of_tiers
        else:
            num_of_tiers = self._num_of_tiers

        portions_list = []
        quotient = num_of_tiers // percoms_amount
        residue = num_of_tiers % percoms_amount

        for i in range(percoms_amount):
            portions_list.append(quotient)

        if residue:
            for i in range(percoms_amount - residue, percoms_amount):
                portions_list[i] += 1

        return portions_list

    def _distribute_percoms(self, percoms_list: List[IDriver]) -> List[List[IDriver]]:
        """Метод распределяет периферийные устройства по ярусам."""

        # Вычисление количества периферийных устройств.
        num = len(percoms_list)

        if not num:
            print("Список пуст.")
            exit(1)

        # Список словарей на каждый ярус.
        objects_per_tier_list = []

        for t in range(self._num_of_tiers):
            objects_per_tier_list.append([])

        calculated = self._calculate_percom_portions(num)
        calculated_index = 0

        for i in range(self._num_of_tiers):
            if self._num_of_tiers >= num:
                # Каждый объект на вычисленное количество ярусов.
                objects_per_tier_list[i].append(percoms_list[calculated_index])
                calculated[calculated_index] -= 1

                if not calculated[calculated_index]:
                    calculated_index += 1
            else:
                # На каждый ярус вычисленное количество объектов.
                for j in range(calculated[i]):
                    objects_per_tier_list[i].append(percoms_list[j])

                calculated_index += calculated[i]

        return objects_per_tier_list

    def load_percoms(self):
        """Метод подключает периферийные устройства."""
        self._io_utils.import_modules(self._get_percom_list(), self._percoms_directory)

        for percom in self._percoms_installed:
            tmp = self._globals_var[self._module_match_list[percom[0]]]

            # Создание объектов подключенных модулей.
            obj_to_list = tmp.Driver(*percom)
            obj_type = obj_to_list.get_type()

            if obj_type is PercomTypeEnum.RELAY:
                for i in RelayChannelTypeEnum:
                    channel_type_list = obj_to_list.get_channel_type_list(i)

                    # В список типов устройств добавляются кортежи из
                    # объектов и каналов подключения.
                    if len(channel_type_list):
                        for ch in channel_type_list:
                            self._relays_list[i].append((obj_to_list, ch))
            elif obj_type is PercomTypeEnum.SENSOR:
                for sensor_type in obj_to_list.get_sensor_type():
                    self._sensors_list[sensor_type].append(obj_to_list)
            else:
                print(f"There is no such type of percom: {obj_type}.")
                exit(1)

    def get_tier_objects(self, tier_number: int) -> Dict:
        """Метод возвращает словарь со списком объектов яруса."""
        if tier_number < 0 or tier_number >= self._num_of_tiers:
            print(f"Номер яруса ({tier_number}) указан неверно.")
            exit(1)

        tier_dict = {}
        for k, v in self._tier_objects_list.items():
            tier_dict[k] = v[tier_number]

        return tier_dict


class TimeService(metaclass=SingletonBaseClass):
    """Класс представляет средства работы со временем."""

    # f24h = Full 24 Hours (Сутки).
    _minutes_per_f24h = 1440
    _minutes_per_hour = 60

    _seconds_per_f24h = 86400
    _seconds_per_hour = 3600
    _seconds_per_minute = 60

    _f24h_start: int  # timestamp начала суток.
    _f24h_end: int  # timestamp конца суток.

    _day_start: int  # timestamp начала дня.
    _previous_night_start: int  # timestamp начала ночи предыдущего дня.
    _night_start: int  # timestamp начала ночи.

    _day_duration: int = 43200  # Продолжительность дня в секундах.
    _night_duration: int  # Продолжительность ночи в секундах.

    def __init__(self) -> None:
        self._techmap_directory = config.techmap_directory
        self._calculate_f24h_values()

    def set_day_duration(self, day_duration: int):
        """Метод утанавливает продолжительность дня."""
        if day_duration > self._minutes_per_f24h or 0 > day_duration:
            print(f"Длина дня указана неверно: {day_duration}")
            exit(1)

        self._day_duration = day_duration * self._seconds_per_minute
        self._calculate_f24h_values()

    def begin_next_day(self) -> None:
        """Метод необходимо запускать при смене суток."""
        self._calculate_f24h_values()

    def _calculate_f24h_values(self) -> None:
        """Метод вычисляет котрольные точки текущих суток."""
        local_time = localtime()

        self._f24h_start = int(mktime((
            local_time.tm_year,
            local_time.tm_mon,
            local_time.tm_mday,
            0,
            0,
            0,
            local_time.tm_wday,
            local_time.tm_yday,
            local_time.tm_isdst
        )))

        self._f24h_end = int(mktime((
            local_time.tm_year,
            local_time.tm_mon,
            local_time.tm_mday,
            23,
            59,
            59,
            local_time.tm_wday,
            local_time.tm_yday,
            local_time.tm_isdst
        )))

        self._day_start = self._f24h_start + (self._seconds_per_f24h - self._day_duration) // 2
        self._night_start = self._day_start + self._day_duration
        self._previous_night_start = self._night_start - self._seconds_per_f24h
        self._night_duration = self._seconds_per_f24h - self._day_duration

    def num_of_days_from_start(self, initial_day: int) -> int:
        """Метод возвращает число дней, прошедших от указанного дня."""

        # Для округления используется int, аналогичный math.floor.
        return int((time() - initial_day) / self._seconds_per_f24h)

    def get_timestamp(self):
        """Функция, считывающая из файла время начала периода."""
        ts_path = self._techmap_directory + '/timestamp.tmp'

        try:
            with open(ts_path, 'r') as ts_file:
                ts = int(ts_file.read())

            return ts
        except FileNotFoundError:
            self.set_timestamp()  # Если файла нет, его необходимо создать.

        return self.get_timestamp()

    def set_timestamp(self, timestamp: int = int(time())) -> None:
        """Функция, сохраняющая в файл время начала периода."""
        ts_path = self._techmap_directory + '/timestamp.tmp'

        with open(ts_path, 'w') as ts_file:
            ts_file.write(str(timestamp))

    def is_a_day(self) -> bool:
        """Метод определяет, является ли текущее время дневным."""
        if self._day_start <= time() < self._night_start:
            return True

        return False

    def calculate_periods_boundaries(self, num_of_periods: int,
                                     periods_duration_in_minutes: int,
                                     time_of_a_day: TimeOfADay = TimeOfADay.F24H
                                     ) -> List[Dict]:
        """Функция вычисляет время начала и окончания периодов.
        Параметр periods_duration принимает значения в минутах.
        """

        # Временной сдвиг в секундах.
        if time_of_a_day == TimeOfADay.F24H:
            start_time = self._f24h_start
            time_shift = self._seconds_per_f24h // num_of_periods
        elif time_of_a_day == TimeOfADay.DAY:
            start_time = self._day_start
            print()
            time_shift = self._day_duration // num_of_periods
        else:
            if self.is_a_day():
                start_time = self._previous_night_start
            else:
                start_time = self._night_start

            time_shift = self._night_duration // num_of_periods

        start_time += (time_shift - periods_duration_in_minutes * self._seconds_per_minute) // 2

        time_boundaries = []

        for p in range(num_of_periods):
            time_period = {
                'start_time': [],
                'end_time': []
            }

            end_time = start_time + periods_duration_in_minutes * self._seconds_per_minute

            # Время начала и окончания периода (timestamp).
            time_period['start_time'] = [
                localtime(start_time).tm_hour,
                localtime(start_time).tm_min
            ]
            time_period['end_time'] = [
                localtime(end_time).tm_hour,
                localtime(end_time).tm_min
            ]

            time_boundaries.append(time_period)

            start_time += time_shift

        return time_boundaries


class TechmapHandler:
    """Класс управления технологической картой."""
    _technological_map: dict

    _current_day: int
    _current_phase: int = 0
    _day_of_phase: int

    # Инвариантные значения.
    _initial_day: int
    _num_of_phases: int
    _total_days: int = 0

    def __init__(self, time_service: TimeService):
        self._time_service = time_service
        self._techmap_directory = config.techmap_directory
        self._current_techmap = config.current_techmap
        self.load_techmap()

    def load_techmap(self, tm_name: str = '') -> None:
        tm_name = tm_name or self._current_techmap
        tm_path = self._techmap_directory + '/' + tm_name + '.csv'
        self._technological_map = IOUtils.csv_reader(tm_path, ',')

        # Приведение параметров технологической карты к типу int.
        for key, value in self._technological_map.items():
            self._technological_map[key] = list(map(int, value))

        self._initial_day = self._time_service.get_timestamp()
        self._num_of_phases = len(self._technological_map['phase_duration'])
        self._current_day = self._time_service.num_of_days_from_start(self._initial_day)

        self._day_of_phase = self._current_day

        for phase_days in self._technological_map['phase_duration']:
            self._total_days += phase_days

            if not self._day_of_phase < phase_days:
                self._day_of_phase -= phase_days
                self._current_phase += 1

        # Проверка на выход за жизненный цикл растения.
        if self._day_of_phase > self._technological_map['phase_duration'][-1]:
            self._day_of_phase = -1

    def begin_next_day(self) -> None:
        """Метод увеличивает текущий день технологической карты."""
        self._current_day += 1

        # Проверка на конец жизненного цикла растения.
        if self._current_phase == -1:
            return

        if self._day_of_phase < self._technological_map['phase_duration'][self._current_phase] - 1:
            self._day_of_phase += 1
        else:
            if self._current_phase < self._num_of_phases - 1:
                self._current_phase += 1
                self._day_of_phase = 0
            else:
                self._current_phase = -1

    def get_current_techmap(self, *parameters: Tuple[str]) -> dict:
        """Метод параметры технологической карты за текущую фазу."""

        current_techmap = {}

        if parameters:
            try:
                for p in parameters:
                    current_techmap[p] = self._technological_map[p][self._current_phase]
            except KeyError as e:
                print(f"Нет такого параметра: {e}")
                exit(1)
        else:
            for p, v in self._technological_map.items():
                current_techmap[p] = v[self._current_phase]

        return current_techmap

    def get_current_techmap_value(self, parameter: str) -> int:
        """Метод возвращает значение параметра на текущей фазе."""

        if parameter:
            try:
                return self._technological_map[parameter][self._current_phase]
            except KeyError as e:
                print(f"Нет такого параметра: {e}")
                exit(1)


class Cron(metaclass=SingletonBaseClass):
    """Класс отвечает за запуск событий по расписанию."""

    # Список задач в формате:
    # [
    #     ['имя_задачи', [аргументы], [час запуска, минута запуска]]
    # ]
    _cron_task_list = []
    _is_running: bool

    def create_cron_task(self, task_to_run,
                         arguments: list, start_time: list) -> None:
        """Метод добавляет задачу в список планировщика задач."""
        self._cron_task_list.append([task_to_run, arguments, start_time])

    def delete_cron_task(self, task_to_del: str) -> None:
        """Метод удаляет задачу из планировщика задач."""
        for task in range(len(self._cron_task_list)):
            if self._cron_task_list[task][0] == task_to_del:
                del self._cron_task_list[task]

    def start_cron_loop(self) -> None:
        """Метод запускает планировщик задач. Подразумевается работа в
         качестве службы.
         """
        self._is_running = True

        while self._is_running:
            for task in self._cron_task_list:
                current_time = localtime()
                if task[2][0] == current_time.tm_hour and\
                        task[2][1] == current_time.tm_min:
                    task[0](*task[1])

            sleep(30)

    def stop_cron_loop(self) -> None:
        """Метод останавливает планировщик задач."""
        self._is_running = False
