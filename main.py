# -*- coding: utf-8 -*-

import signal
import time
from threading import Thread

from typing import Dict, Union

from modules import enumerations
from modules import interfaces
from modules import utils
import config


class System(metaclass=interfaces.SingletonBaseClass):
    """Базовый класс."""
    _is_running: bool
    _running_threads = []
    _tiers = []  # Доработать.

    def __init__(self):
        self._is_running = True

        # Регистрация обработчиков сигналов прерывания.
        signal.signal(signal.SIGINT, self._signal_handler)
        signal.signal(signal.SIGTSTP, self._exit_handler)

        self._io = utils.IOUtils(globals())
        self._pcs_handler = utils.PercomsHandler(globals(), self._io)
        self._time_service = utils.TimeService()
        self._tcm_handler = utils.TechmapHandler(self._time_service)

        # Установка продолжительности дня из технологической карты.
        self._time_service.set_day_duration(
            self._tcm_handler.get_current_techmap_value('lighting_duration')
        )

        # Создание ярусов.
        for tier_num in range(config.num_of_tiers):
            self._tiers.append(Tier(self._pcs_handler.get_tier_objects(tier_num),
                                    tier_num, self._tcm_handler, self._time_service))

        self._cron = utils.Cron()

        # Добавление заданий в планировщик.
        self._cron.create_cron_task(self._time_service.begin_next_day, [], [0, 0])
        self._cron.create_cron_task(self._tcm_handler.begin_next_day, [], [0, 0])

        for tier in self._tiers:
            tier.schedule_tasks(tier, self._cron)

            # Добавление сервиса контроля яруса в список потоков.
            self._running_threads.append(
                Thread(target=tier.start_check_tier_loop, args='')
            )

        # Добавление фоновых методов в список потоков.
        self._running_threads.append(
            Thread(target=self._cron.start_cron_loop, args='')
        )

        self._start_threads_execution()

        print('Для завершения работы введите "exit"')
        while self._is_running:
            if input() == 'exit':
                self.shutdown()

    def _start_threads_execution(self) -> None:
        """Метод запускает функции в параллельных потоках."""
        for t in self._running_threads:
            t.daemon = True
            t.start()

    def _stop_threads_execution(self) -> None:
        """Метод останавиливает работающие потоки."""
        for t in self._running_threads:
            t.join()

    def shutdown(self):
        """Функция завершения работы программы."""
        print("Завершение работы программы...")
        for tier in self._tiers:
            tier.stop_check_tier_loop()
        self._cron.stop_cron_loop()
        self._stop_threads_execution()
        self._is_running = False
        exit(0)

    def _signal_handler(self, signum, frame):
        """Обработчик прерывания Ctrl + C."""
        print("Signal Number:", signum, " Frame: ", frame)
        self.shutdown()

    def _exit_handler(self, signum, frame):
        """Обработчик прерывания Ctrl + Z."""
        print("Signal Number:", signum, " Frame: ", frame)
        self.shutdown()


class Tier:
    """Класс управления ярусом."""
    _percoms: dict  # Список периферийных устройств.
    _tier_num: int  # Номер яруса.
    _techmap_handler: utils.TechmapHandler
    _time_service: utils.TimeService

    _tier_techmap: dict
    _is_running: bool
    _is_a_day: bool

    _sensors_list = {}

    _sensors_critical_delta = {
        enumerations.SensorTypeEnum.AQUA: 10,
        enumerations.SensorTypeEnum.CARBONE_DIOXIDE: 10,
        enumerations.SensorTypeEnum.HUMIDITY: 10,
        enumerations.SensorTypeEnum.LIGHTING: 10,
        enumerations.SensorTypeEnum.TEMPERATURE: 10
    }

    _sensors_enum_to_str = {
        enumerations.SensorTypeEnum.AQUA: 'aqua',
        enumerations.SensorTypeEnum.CARBONE_DIOXIDE: 'carbone_dioxide',
        enumerations.SensorTypeEnum.HUMIDITY: 'humidity',
        enumerations.SensorTypeEnum.LIGHTING: 'lighting',
        enumerations.SensorTypeEnum.TEMPERATURE: 'temperature'
    }

    def __init__(self, percoms: Dict[Union[
                                         enumerations.SensorTypeEnum,
                                         enumerations.RelayChannelTypeEnum
                                     ], list],
                 tier_num, techmap_handler, time_service: utils.TimeService):
        """Конструктор принимает набор периферийный устройств, номер
        яруса, объект класса TimeService."""
        self._percoms = percoms
        self._tier_num = tier_num
        self._techmap_handler = techmap_handler
        self._time_service = time_service

        # Добавление в список установленных сенсоров.
        for p_type, p_objects in self._percoms.items():
            if type(p_objects[0]) is not tuple:
                self._sensors_list[p_type] = p_objects

        self.set_time_of_a_day()

    def schedule_tasks(self, tier_obj, cron_obj):
        """Метод занимается планировкой задач."""

        # Получение актуальной технологической карты.
        self._tier_techmap = self._techmap_handler.get_current_techmap()

        day_time_watering = self._time_service.calculate_periods_boundaries(
            self._tier_techmap['watering_d_times'],
            self._tier_techmap['watering_d_duration'],
            enumerations.TimeOfADay.DAY
        )

        night_time_watering = self._time_service.calculate_periods_boundaries(
            self._tier_techmap['watering_n_times'],
            self._tier_techmap['watering_n_duration'],
            enumerations.TimeOfADay.NIGHT
        )

        lighting = self._time_service.calculate_periods_boundaries(
            1,
            self._tier_techmap['lighting_duration'],
            enumerations.TimeOfADay.F24H
        )

        for period in day_time_watering:
            cron_obj.create_cron_task(tier_obj.turn_pump_on, [], period['start_time'])
            cron_obj.create_cron_task(tier_obj.turn_pump_off, [], period['end_time'])

        for period in night_time_watering:
            cron_obj.create_cron_task(tier_obj.turn_pump_on, [], period['start_time'])
            cron_obj.create_cron_task(tier_obj.turn_pump_off, [], period['end_time'])

        for period in lighting:
            cron_obj.create_cron_task(tier_obj.turn_light_on, [], period['start_time'])
            cron_obj.create_cron_task(tier_obj.turn_light_off, [], period['end_time'])
            cron_obj.create_cron_task(tier_obj.set_time_of_a_day, [], period['start_time'])
            cron_obj.create_cron_task(tier_obj.set_time_of_a_day, [], period['end_time'])

    def set_time_of_a_day(self):
        """Метод устанавливает время суток."""
        self._is_a_day = self._time_service.is_a_day()

    def turn_light_on(self):
        """Метод включает освещение яруса."""
        for relay in self._percoms[enumerations.RelayChannelTypeEnum.LIGHT]:
            relay[0].set_relay_channel_status(
                relay[1],
                enumerations.RelayModuleStatusEnum.CLOSED
            )

    def turn_light_off(self):
        """Метод выключает освещение яруса."""
        for relay in self._percoms[enumerations.RelayChannelTypeEnum.LIGHT]:
            relay[0].set_relay_channel_status(
                relay[1],
                enumerations.RelayModuleStatusEnum.OPEN
            )

    def turn_pump_on(self):
        """Метод включает помпу яруса."""
        for relay in self._percoms[enumerations.RelayChannelTypeEnum.PUMP]:
            relay[0].set_relay_channel_status(
                relay[1],
                enumerations.RelayModuleStatusEnum.CLOSED
            )

    def turn_pump_off(self):
        """Метод выключает помпу яруса."""
        for relay in self._percoms[enumerations.RelayChannelTypeEnum.PUMP]:
            relay[0].set_relay_channel_status(
                relay[1],
                enumerations.RelayModuleStatusEnum.OPEN
            )

    def _check_sensors(self, sensor_type: enumerations.SensorTypeEnum) -> enumerations.SensorStatus:
        """Метод проверяет показатели сенсоров."""
        num_of_sensors = 0
        average_value = 0

        for s in self._sensors_list[sensor_type]:
            check_function = getattr(s, 'get_' + self._sensors_enum_to_str[sensor_type])

            average_value += check_function()
            num_of_sensors += 1

        average_value /= num_of_sensors

        d_min = self._tier_techmap[self._sensors_enum_to_str[sensor_type] + '_d_min']
        d_max = self._tier_techmap[self._sensors_enum_to_str[sensor_type] + '_d_max']
        n_min = self._tier_techmap[self._sensors_enum_to_str[sensor_type] + '_n_min']
        n_max = self._tier_techmap[self._sensors_enum_to_str[sensor_type] + '_n_max']
        delta = self._sensors_critical_delta[sensor_type]

        if self._is_a_day:
            if average_value >= d_min:
                if average_value <= d_max:
                    return enumerations.SensorStatus.NORMAL
                elif average_value > d_max + delta:
                    return enumerations.SensorStatus.CRITICAL_HIGH
                else:
                    return enumerations.SensorStatus.HIGH
            elif average_value < d_min - delta:
                return enumerations.SensorStatus.CRITICAL_LOW
            else:
                return enumerations.SensorStatus.LOW
        else:
            if average_value >= n_min:
                if average_value <= n_max:
                    return enumerations.SensorStatus.NORMAL
                elif average_value > n_max + delta:
                    return enumerations.SensorStatus.CRITICAL_HIGH
                else:
                    return enumerations.SensorStatus.HIGH
            elif average_value < n_min - delta:
                return enumerations.SensorStatus.CRITICAL_LOW
            else:
                return enumerations.SensorStatus.LOW

    def start_check_tier_loop(self):
        """Метод опрашивает датчики. В случае расхождения с
        требуемыми значениями включает необходимые узлы.
        """
        self._is_running = True

        while self._is_running:
            for s_type in self._sensors_list.keys():
                status = self._check_sensors(s_type)

                # Реализованы не все типы сенсоров.
                if s_type is enumerations.SensorTypeEnum.HUMIDITY:
                    if status is enumerations.SensorStatus.NORMAL:
                        # В последующих итерациях реализовать через флаги.
                        self.turn_pump_off()
                    elif status is enumerations.SensorStatus.LOW:
                        self.turn_pump_on()
                    elif status is enumerations.SensorStatus.CRITICAL_LOW:
                        print(f"На ярусе №{self._tier_num} показатели"
                              f" влажности достигли критически низкой отметки!")
                        self.turn_pump_on()
                    elif status is enumerations.SensorStatus.HIGH:
                        self.turn_pump_off()
                        # Реализовать включение вытяжки.
                    else:
                        self.turn_pump_off()
                        # Реализовать включение вытяжки.
                        print(f"На ярусе №{self._tier_num} показатели"
                              f" влажности достигли критически высокой отметки!")
                elif s_type is enumerations.SensorTypeEnum.TEMPERATURE:
                    # Необходимо реализовать работу вытяжки.
                    if status is enumerations.SensorStatus.NORMAL:
                        # В последующих итерациях реализовать через флаги.
                        # Реализовать работу вытяжки.
                        ...
                    elif status is enumerations.SensorStatus.LOW:
                        # Реализовать работу вытяжки.
                        ...
                    elif status is enumerations.SensorStatus.CRITICAL_LOW:
                        # Реализовать работу вытяжки.
                        print(f"На ярусе №{self._tier_num} показатели"
                              f" температуры достигли критически низкой отметки!")
                    elif status is enumerations.SensorStatus.HIGH:
                        # Реализовать работу вытяжки.
                        ...
                    else:
                        # Реализовать работу вытяжки.
                        print(f"На ярусе №{self._tier_num} показатели"
                              f" температуры достигли критически высокой отметки!")

            time.sleep(1)

    def stop_check_tier_loop(self):
        """Метод останавливает службу контроля яруса."""
        self._is_running = False


if __name__ == '__main__':
    system = System()
